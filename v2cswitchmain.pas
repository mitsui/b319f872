unit V2CSwitchMain;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls, ComCtrls,
  ExtCtrls, FileUtil, Process;

type

  { TFormV2CSwitchMain }

  TFormV2CSwitchMain = class(TForm)
    ButtonExit: TButton;
    ButtonMinimum: TButton;
    ButtonRemove: TButton;
    ButtonStop: TButton;
    ButtonRun: TButton;
    ListBoxConfigFiles: TListBox;
    StatusBarRunningStatus: TStatusBar;
    TimerV2rayWatch: TTimer;
    TrayIconDefaultTray: TTrayIcon;
    procedure ButtonExitClick(Sender: TObject);
    procedure ButtonMinimumClick(Sender: TObject);
    procedure ButtonRemoveClick(Sender: TObject);
    procedure ButtonRunClick(Sender: TObject);
    procedure ButtonStopClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure TimerV2rayWatchTimer(Sender: TObject);
    procedure TrayIconDefaultTrayClick(Sender: TObject);
  private

  public

  end;

var
  FormV2CSwitchMain: TFormV2CSwitchMain;
  V2RayProcess: TProcess;
  ApplicationRoot: String;

implementation

function GetConfigFileList(): TStrings;
var
  FullFilenameList: TStrings;
  ExtractedFileName: String;
  f: String;
begin
  FullFilenameList:=TStringList.Create;
  GetConfigFileList:=TStringList.Create;
  FindAllFiles(FullFilenameList, ConcatPaths([ApplicationRoot, 'Configs']), '*.json', False);
  for f in FullFilenameList do
  begin
    ExtractedFileName:=ExtractFileName(f);
    if ExtractedFileName.Length < 6 then continue;
    GetConfigFileList.Add(ExtractedFileName.Substring(0, ExtractedFileName.Length - 5));
  end;
end;

{$R *.lfm}

{ TFormV2CSwitchMain }

procedure TFormV2CSwitchMain.ButtonExitClick(Sender: TObject);
begin
  FormV2CSwitchMain.Close;
end;

procedure TFormV2CSwitchMain.ButtonMinimumClick(Sender: TObject);
begin
  TrayIconDefaultTray.Visible:=True;
  FormV2CSwitchMain.Hide;
end;

procedure TFormV2CSwitchMain.ButtonRemoveClick(Sender: TObject);
begin
  if ListBoxConfigFiles.ItemIndex <> -1 then
  begin
    if QuestionDlg('确认删除','同时删除配置文件吗？否则只从列表移除，重启软件恢复', mtConfirmation, [mrYes, '是', mrNo, '否'], '') = mrYes then
      DeleteFile(ConcatPaths([ApplicationRoot, 'Configs', ListBoxConfigFiles.GetSelectedText + '.json']));
    ListBoxConfigFiles.Items.Delete(ListBoxConfigFiles.ItemIndex);
  end;
end;

procedure TFormV2CSwitchMain.ButtonRunClick(Sender: TObject);
begin
  if ListBoxConfigFiles.ItemIndex <> -1 then
  begin
    with V2RayProcess do
    begin
      if Running then Terminate(0);
      Parameters.Clear;
      Parameters.Add('-config');
      Parameters.Add(ConcatPaths([ApplicationRoot, 'Configs', ListBoxConfigFiles.GetSelectedText + '.json']));
      Execute;
    end;
    if not TimerV2rayWatch.Enabled then TimerV2rayWatch.Enabled:=True;
  end;
end;

procedure TFormV2CSwitchMain.ButtonStopClick(Sender: TObject);
begin
  if V2RayProcess.Running then V2RayProcess.Terminate(0);
end;

procedure TFormV2CSwitchMain.FormClose(Sender: TObject;
  var CloseAction: TCloseAction);
begin
  if V2RayProcess.Running then V2RayProcess.Terminate(0);
end;

procedure TFormV2CSwitchMain.FormCreate(Sender: TObject);
begin
  ApplicationRoot:=ExtractFileDir(Application.ExeName);
  ListBoxConfigFiles.Items:=GetConfigFileList;
  V2RayProcess:=TProcess.Create(Nil);
  with V2RayProcess do
  begin
    Executable:=ConcatPaths([ApplicationRoot, 'VCore', 'v2.exe']);
    InheritHandles:=False;
    Environment.Add('V2RAY_LOCATION_ASSET='+ConcatPaths([ApplicationRoot, 'VCore']));
    Environment.Add('XRAY_LOCATION_ASSET='+ConcatPaths([ApplicationRoot, 'VCore']));
    Environment.Add('SystemRoot=C:\Windows');
    Options:=Options + [poNoConsole];
    {for I := 1 to GetEnvironmentVariableCount do Environment.Add(GetEnvironmentString(I))}
  end;
  { V2RayProcess.Options:=V2RayProcess.Options + [poNewConsole]; }
end;

procedure TFormV2CSwitchMain.TimerV2rayWatchTimer(Sender: TObject);
begin
  if V2RayProcess.Running then StatusBarRunningStatus.SimpleText:='正在运行'
  else
  begin
    StatusBarRunningStatus.SimpleText:='已停止';
    TimerV2rayWatch.Enabled:=False;
  end;
end;

procedure TFormV2CSwitchMain.TrayIconDefaultTrayClick(Sender: TObject);
begin
  TrayIconDefaultTray.Visible:=False;
  FormV2CSwitchMain.Show;
end;

end.

